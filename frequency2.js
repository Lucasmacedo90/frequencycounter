
   
   document.getElementById("countButton").onclick = function () {
      let typedText = document.getElementById("textInput").value;
   // receber a contage de letras
   const letterCounts = {};
   // receber a contagem de palavras
   const wordCounts = {}
   // minusculo, para contar Caes e caes
   typedText = typedText.toLowerCase();
   // quebrar em palavras o texto
   typedText = typedText.replace(/[^a-z'\s]+/g, "");
   // quebrar em palavras o texto
   words = typedText.split(/\s/);
//   contagem de letras
for (let i = 0; i < typedText.length; i += 1) {
   currentLetter = typedText[i];
   if (letterCounts[currentLetter] === undefined) {
      letterCounts[currentLetter] = 1;
   } else {
      letterCounts[currentLetter]++;
   }
}
// contagem de palavras
for (let j = 0; j < words.length; j++) {
   currentWord = words[j];
   // faça algo com cada palavra
   if (wordCounts[currentWord] === undefined) {
      wordCounts[currentWord] = 1;
   } else {
      wordCounts[currentWord]++;
   }
}
// imprimir a contagem de letras no html
for (let letter in letterCounts) {
   const span = document.createElement("span");
   const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
   span.appendChild(textContent);
   document.getElementById("lettersDiv").appendChild(span);
}
// imprimir contagem de palavras no html 
for (let word in wordCounts) {
   const span = document.createElement("span");
   const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
   span.appendChild(textContent);
   document.getElementById("wordsDiv").appendChild(span);
}
}