const typedText = document.getElementById("textInput").value;
    const letterCounts = {};
    const wordCounts = {};
    words = typedText.split(/\s/);
document.getElementById("countButton").onclick = function() {  
    // teu código vai aqui ...
    typedText = typedText.toLowerCase(); 
    typedText = typedText.replace(/[^a-z'\s]+/g, ""); 
    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];
        // faça algo com cada letra 
        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1; 
         } else { 
            letterCounts[currentLetter]++; 
         } 
        }
        words = typedText.split(/\s/);
        for (let j = 0; j < typedText.length; j++) {
            currentWord = words[j];
            // faça algo com cada letra 
            if (wordCounts[currentWord] === undefined) {
                wordCounts[currentWord] = 1; 
             } else { 
                wordCounts[currentWord]++; 
             } 
            }  
}
     for (let letter in letterCounts) { 
        const span = document.createElement("span"); 
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", "); 
        span.appendChild(textContent); 
        document.getElementById("lettersDiv").appendChild(span); 
     }
     for (let word in wordCounts) { 
        const span = document.createElement("span"); 
        const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", "); 
        span.appendChild(textContent); 
        document.getElementById("wordsDiv").appendChild(span); 
     }